import React, { Component } from 'react';
import axios from 'axios';

import { StatusBar } from 'expo-status-bar';
import {
  StyleSheet, Text, View,
  Image,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  FlatList,
} from 'react-native';

class ChatApp extends Component {
  constructor() {
    super();
    this.state = {
      message: '',
      chat: [{user: 'Bot', text: "Hello! I'm Neomind, your trusted companion in the realm of meditation."}],
    };
  }

  // Function to send a message
  sendMessage = async () => {
    const newMessage = this.state.message;

    // Update the chat with the new message
    this.setState(prevState => ({
      chat: [{ user: 'You', text: newMessage }, ...prevState.chat,],
      message: '',
    }));

    // Send the message to the API
    try {
      const response = await axios.post('https://app-gpt.onrender.com/gpttest', {
        data: newMessage,
      } );

      const botResponse = response.data;
    
      // Update the chat with the API response
      this.setState(prevState => ({
        chat: [{ user: 'Bot', text: botResponse }, ...prevState.chat,],
      }));
    } catch (error) {
      console.error('API Request Error:', error);
    }
  }

  renderItem = ({ item }) => (item.user === "You" ?
    <View style={styles.bubble_user} >
      <Text style={{ color: "white" }}>{item.text}</Text>
    </View>
    :
    <View style={styles.message_gpt}>
      <Image style={styles.profile_image} source={require('./pictures/profile.png')} />

      <View style={styles.bubble}>
        <Text style={{ color: "white" }}>{item.text}</Text>
      </View>

    </View>
  );

  render() {
    return (
      <View style={styles.background}>
        <StatusBar style="light" />
        <KeyboardAvoidingView behavior='position'>
          <FlatList
            data={this.state.chat}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index.toString()}

            inverted={true}
          />

          <View style={{ flexDirection: "row", marginBottom: 20, justifyContent: "space-around" }}>
            <TextInput
              style={styles.text_input}
              placeholder='Your message'
              placeholderTextColor="white"
              value={this.state.message}
              onChangeText={(text) => this.setState({ message: text })}
            />

            <TouchableOpacity onPress={this.sendMessage} style={styles.send_button}>
              <Image
                style={styles.send_image_size}
                source={require('./pictures/Send.png')}

              />
            </TouchableOpacity>
          </View>

        </KeyboardAvoidingView>


      </View>
    );
  }
}

export default ChatApp;


const styles = StyleSheet.create({
  background: {
    backgroundColor: 'rgb(2, 7, 36)',
    alignItems: 'center',
    justifyContent: 'center',
  },

  send_button: {
    backgroundColor: '#FFF',
    borderRadius: 10,
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },

  send_image_size: {
    height: 25,
    resizeMode: 'contain',
  },

  text_input: {
    borderWidth: 1,
    borderColor: 'rgba(255, 255, 255, 0.50)',
    backgroundColor: 'rgba(255, 255, 255, 0.10)',
    padding: 8,
    width: '80%',
    borderRadius: 10,
    color: 'white',
    height: 50,
    marginRight: '2%',


  },

  bubble: {
    backgroundColor: 'rgba(89, 107, 193, 1)',
    padding: 15,
    maxWidth: "70%",
    borderRadius: 12,
    borderEndStartRadius: 2,

  },

  bubble_user: {
    backgroundColor: 'rgba(45, 45, 65, 1)',
    padding: 15,
    maxWidth: "70%",
    borderRadius: 15,
    borderEndEndRadius: 2,
    alignSelf: 'flex-end',

  },

  profile_image: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    borderColor: 'rgba(89, 107, 193, 1)',
    borderWidth: 1,
    borderRadius: 50,
    marginRight: 10,
  },

  message_gpt: {
    flexDirection: "row",
    marginTop: 20,
    alignItems: 'flex-end',
    bottom: 0,
    marginBottom: 20,
  },

  top: {
    marginTop: 100,
  }

});







